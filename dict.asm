%include "lib.inc"
%define word_size 8

global find_word

section .text

find_word:
    .loop:      
        push rdi       
        push rsi
        add rsi, word_size     
        call string_equals

        pop rsi
        pop rdi

        cmp rax, 1
        je .true 

        mov rsi, [rsi]
        test rsi, rsi
        jne .loop
        jmp .false
    
    .true:
        mov rax, rsi
        ret
    .false:
        xor rax, rax
        ret
