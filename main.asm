section .rodata

long_word_mes: db 'Error: Word length should be less than 255', 10, 0
not_found_mes: db 'Error: There is no such word in dictionary', 10, 0

section .data

%include "lib.inc"
%include "words.inc"

extern find_word

%define word_size 8
%define string_max_length 256

section .text
global _start

not_found_error:
        mov rdi, not_found_mes
        jmp print_error

long_word_error:
        mov rdi, long_word_mes

print_error:
        call print_string

        xor rdi, rdi
        jmp exit

read_key:
        mov rsi, string_max_length
        mov rdi, rsp
        sub rdi, rsi

        call read_line

        cmp rax, 0
        je long_word_error
        ret

find_key:
        mov rsi, pointer
        call find_word

        cmp rax, 0
        je not_found_error
        ret

_start:
        call read_key

        push rdx

        mov rdi, rax
        call find_key

        pop rdx

        add rax, word_size
        add rax, rdx
        inc rax    
        mov rdi, rax

        call print_string
        call print_newline

        xor rdi, rdi
        jmp exit
