
ASM=nasm
ASMFLAGS=-f elf64
LD=ld
RF=-rf

dictionary: main.o dict.o lib.o
	$(LD) -o $@ $^

.PHONY: clean
clean:
	rm $(RF) *.o main

main.asm: lib.inc words.inc colon.inc
	touch $@

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<
